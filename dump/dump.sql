CREATE TABLE public.short_url
(
    short_url character(100) COLLATE pg_catalog."default",
    long_url character(255) COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('short_url_id_seq'::regclass)
)