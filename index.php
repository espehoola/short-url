<?php

require 'class.php';

$urlObj = new shortUrl();
$url = '';

if(isset($_GET['url'])) {
    $url = $urlObj->getLongUrl($_GET['url']);
}

if(isset($_GET['url']) && $url) {
    header("Location:" . $url);
    die;
}

$url = !empty($_GET['url']) ? $_GET['url'] : '';

echo '<!DOCTYPE html>
<html lang="ru">
<head>
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="js/index.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <meta charset="UTF-8">
    <title>Short URL</title>
</head>
<body>
    <div class="container">
        <div>
            URL:
                <form class="js-form"> 
                <input name="url"><br>
                <input type="submit" value="Create" class="btn btn-primary js-submit">
                </form>
                <br>
                <span class="bg-info js-response">
                
                </span>
            </div>
        </div>
    </div>
</body>
</html>';