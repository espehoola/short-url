<?php

class shortUrl {

    protected static $chars = "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    protected static $table = "short_url";
    

    public function __construct()
    {
        require 'config.php';
        try {
            $this->pdo = new PDO(DB_PDODRIVER . ":host=" . DB_HOST .
                ";dbname=" . DB_DATABASE,DB_USERNAME,DB_PASSWORD);
        }

        catch(PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * Store combinations of short code an orignal URl
     * @param $url string
     * @return bool|string string
     */
    public function storeShortUrl($url)
    {
        $code = '';

        if (empty($url)) {
            echo "URL can't be empty";
            return false;
        }

        $query = "SELECT short_url FROM " . self::$table . " WHERE long_url = ?";
        $stmnt = $this->pdo->prepare($query);
        $stmnt->execute(array($url));

        $data = $stmnt->fetch();

        if(!empty($data)) {

            return DOMAIN . $data['short_url'];
        }

        for($i = 1; $i <= 6; $i++) {
            $code .= self::$chars[rand(0, 60)];
        }

        $query = "INSERT INTO " . self::$table . " (short_url, long_url) VALUES (?, ?)";
        $this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $stmnt = $this->pdo->prepare($query);
        $data = [$code, $url];
        $stmnt->execute($data);

        return DOMAIN . $code;
    }

    /**
     * get original URL by short code
     * @param $code string
     * @return bool|$long_url original URL
     */
    public function getLongUrl($code)
    {
        $query = "SELECT long_url FROM " . self::$table . " WHERE short_url= ?";
        $stmnt = $this->pdo->prepare($query);
        $stmnt->execute(array($code));
        $row = $stmnt->fetch(PDO::FETCH_ASSOC);

        if($row['long_url'] != '' ) {
            return $row['long_url'];
        }

        return false;
    }
}